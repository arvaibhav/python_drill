def is_prime(n):
    """
    Check whether a number is prime or not
    """
    for i in range(2,n):  
        if (n % i) == 0:  
            return False
            break
    else: 
        return True;



def n_digit_primes(digit=2):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    a=[]
    if digit==2:
        for x in range (10, 100):
            test=is_prime(x)
            if test==True:
                a.append(x)
        return a

    if digit ==1:
        for x in range (2, 10):
            test=is_prime(x)
            if test==True:
                a.append(x)
        return a



