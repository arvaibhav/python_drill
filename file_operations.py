"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    x=''
    with open(path) as f:
        x=f.readline()
    return x


def write_to_file(path, s):
    x=open(path,'w')
    x.write(s)
    x.close()



def append_to_file(path, s):
    x = open(path, 'w')
    x.write(s)
    x.close()


def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    f=open(file_path,'w')
    for i in range(1,n+1):
        f.write('{0},{1}"\n"'.format(i,i**2))
        print("{0},{1}".format(i,i**2))
    return f
