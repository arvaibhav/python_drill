def unique(items):
    """
    Return a set of unique values belonging to the `items` list
    """
    return set(items)


def shuffle(items):
    """
    Shuffle all items in a list
    """
    return shuffle(items)


def getcwd():
    """
    Get current working directory
    """
    import os
    cwd = os.getcwd()
    return cwd



def mkdir(name):
    """
    Create a directory at the current working directory
    """
    import os
    os.mkdir(name)

