def last_3_characters(x):
    return x[-3:]


def first_10_characters(x):
    return x[:10]


def chars_4_through_10(x):
    return x[:10][3];


def str_length(x):
    return len(x)


def words(x):
    return x;


def capitalize(x):
    return x.capitalize()


def to_uppercase(x):
    return x.upper();
