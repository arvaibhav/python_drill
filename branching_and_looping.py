def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    my_list =[]
    for i in range(start, end, step):
        my_list.append(i)
    return my_list

def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    my_list = []
    i=start
    if(start<end):
        while (i < end):
            my_list.append(i)
            i += step
        return my_list
    else:
        while (i > end):
            my_list.append(i)
            i += step
        return my_list


def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    my_list=[]
    for i in range(10,100,1):
        is_prime=True
        for j in range(2,i,1):
            if(i%j==0):
                is_prime=False
                break
        if(is_prime==True):
             my_list.append(i)

    return my_list
